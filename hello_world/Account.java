public class Account {
    private double balance;
    private String name;

    private static double interestRate = 4; 
    //constructors

    public Account(double b, String s) {
        balance = b;
        name = s;
    }

    public Account() {
        this(50, "Katharine");
    }

    // getters and setters
    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }


    public void addInterest(){
        balance = (balance*interestRate);
    }

    public boolean withdraw(double amount){
        if (amount < balance){
            return true;
        }
        else {
            return false;
        }
    }

    public boolean withdraw(){
        double amount = balance - 20;
        if (amount < 0){
            return true;
        }
        else {
            return false;
        }
    }

    
}