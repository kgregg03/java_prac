public class TestAccount {
    public static void main(String[] args) {
        Account[] arrayOfAccounts;
        arrayOfAccounts = new Account[5];

        double[] amounts = {23, 89, 109, 596, 234};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};

        for (int i=0; i<arrayOfAccounts.length; i++){
            arrayOfAccounts[i] = new Account();
            arrayOfAccounts[i].setName(names[i]);
            arrayOfAccounts[i].setBalance(amounts[i]);

            System.out.println(arrayOfAccounts[i].getName() + " has " + arrayOfAccounts[i].getBalance());

            arrayOfAccounts[i].addInterest();
            System.out.println("The new balance is " + arrayOfAccounts[i].getBalance());
        }

        Account myAccount = new Account();
        myAccount.setBalance(10000);
        myAccount.setName("Katharine");

        System.out.println("My name is " + myAccount.getName() + " and I have £" + myAccount.getBalance());

        myAccount.addInterest();
        System.out.println("I now have £" + myAccount.getBalance());

        
    }
}
