public class TestAccount2 {
    public static void main(String[] args) {
        Account[] myArrayAcc = new Account[5];

        double[] amounts = {23, 89, 109, 596, 234};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};
        
        for (int i=0; i<myArrayAcc.length; i++){
            myArrayAcc[i] = new Account(amounts[i], names[i]);

            System.out.println("Balance = " + myArrayAcc[i].getBalance());
            myArrayAcc[i].addInterest();
            System.out.println("New balance = " + myArrayAcc[i].getBalance());
            System.out.println();
        }
    }
}
